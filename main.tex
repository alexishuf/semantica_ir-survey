\documentclass[12pt]{article}
\usepackage{sbc-template}
\newcommand{\mytitle}[0]{Automatic Service Composition with preconditions and effects: a brief survey}
\usepackage{graphicx,url}
\usepackage[english]{babel}   
\usepackage[utf8]{inputenc}  
\usepackage{lipsum}
\usepackage{xspace}
\usepackage{color}
\usepackage{booktabs}
\usepackage{hyperref}
\renewcommand{\sectionautorefname}{Section}
\renewcommand{\subsectionautorefname}{Subsection}
\renewcommand{\subsubsectionautorefname}{Subsubsection}
\hypersetup{
  pdftitle={\mytitle},
  pdfauthor={Alexis Huf},
%  pdfsubject={},
%  pdfkeywords={service composition; REST; publish-subscribe; plan repair; SPARQL; preconditions; effects},
%  pdfcreator={LaTeX},
  colorlinks=true,
  linkcolor=black,
  citecolor=black,
  % filecolor=black,
  urlcolor=black,
}

\usepackage{minted}
\definecolor{bgMinted}{rgb}{0.92,0.92,0.92}
\setminted{linenos,bgcolor=bgMinted,fontsize=\footnotesize}
\newmintinline[mt]{turtle}{fontsize=\footnotesize,escapeinside=||}
\newmintinline[ms]{sparql}{fontsize=\footnotesize,escapeinside=||}

\usepackage{soulutf8}
\usepackage{todonotes}
\usepackage{glossaries}
\glsdisablehyper
\makeatletter
\newcommand{\xxnewacronym}[4][]{%
  \newacronym[#1]{#2}{#3}{#4}%
  \@namedef{#2}{\gls{#2}\xspace}%
  \@namedef{#2s}{\glspl{#2}\xspace}%
}
\newcommand{\xnewacronym}[3][]{\xxnewacronym[#1]{#2}{#2}{#3}}
\makeatother
\renewrobustcmd*{\glsfirst}[2][]{\glsreset{#2}\gls[#1]{#2}}
\renewrobustcmd*{\Glsfirst}[2][]{\glsreset{#2}\Gls[#1]{#2}}
\renewrobustcmd*{\GLSfirst}[2][]{\glsreset{#2}\GLS[#1]{#2}}
\renewrobustcmd*{\glsfirstplural}[2][]{\glsreset{#2}\glspl[#1]{#2}}
\renewrobustcmd*{\Glsfirstplural}[2][]{\glsreset{#2}\Glspl[#1]{#2}}
\renewrobustcmd*{\GLSfirstplural}[2][]{\glsreset{#2}\GLSpl[#1]{#2}}
\xnewacronym{ASC}{Automatic Service Composition}
\xnewacronym{BPEL}{Business Process Execution Language}
\xnewacronym{DL}{Description Logic}
\xnewacronym{DDL}{Dynamic Description Logic}
\xnewacronym{FOL}{First Order Logic}
\xnewacronym{HATEOAS}{Hypermedia As The Engine Of Application State}
\xnewacronym{HTN}{Hierarchical Task Network}
\xxnewacronym[longplural={Inputs/Outputs}]{IO}{I/O}{Input/Outputs}\glsunset{IO}
\xnewacronym[longplural={Non-Functional Properties}]{NFP}{Non-Functional Property}
\xnewacronym{OWL}{Web Ontology Language}
\xnewacronym{SLR}{Systematic Literature Review}
\xnewacronym{SPARQL}{SPARQL Query Language}
\xnewacronym{REST}{Representational State Transfer}
\xnewacronym{RDF}{Resource Description Framework}
\xnewacronym{RQ}{Research Question}

     
\sloppy

\title{\mytitle}
\author{Alexis Huf\inst{1}}
\address{PPGCC - UFSC}

\begin{document} 

\maketitle

\begin{abstract}
Automated Service Composition is a problem where, given a set of services and a goal, an algorithm builds a workflow that achieves the desired goal by invoking a subset of the given services. 
Usually, composition algorithms rely on description of inputs and outputs of services or on \textit{a priori} rules that describe how to break complex tasks into simpler tasks. These approaches limit the set of composition problems that can be solved by these algorithms. 
To overcome this limitations, some approaches employ additional formalisms that describe preconditions and effects of each service. This paper is a short survey on composition approaches that support preconditions and effects of services beyond the simple specification of inputs and outputs.
\end{abstract}
     
\section{Introduction}

%## Define composition
Services are computational entities able to perform a function upon request. They have three fundamental properties: access through standardized technology; loose coupling; and discoverable definition and location \cite{Papazoglou2003}. The aforementioned properties enable the construction (and delivery) of new services, given requirements and a set of existing services, a process termed service composition. Such composite services can be designed manually by human specialists or automatically by algorithms, the latter process being named \glsfirst{ASC}

%## Contrast preconditions and effects with I/O-only
\ASC methods vary not only in the particular methods employed, but also on which types of requirements are supported. The description of both existing and desired composite services as adopted by most \ASC methods can be abstracted by an $\langle Inputs,\ Outputs,\ Preconditions,\ Effects,\ NFPs,\ Ontology \rangle$ 6-tuple \cite{Fanjiang2017}. Inputs and Outputs are analogous to their homonyms in programming languages, with the difference that the types are described by some form of ontology shared between services and clients. \NFPs, in the context of \ASC mainly include numeric attributes to be optimized or kept within a given interval, such as response time and cost. %Altough less common, it is also possible to consider non-numeric \NFPs, such as trust, in service composition \cite{Paradesi2009}.  
Precondition and effects have a larger degree of variation among \ASC methods, but most frequently are expressed with a logic variant (e.g., Description Logics or First-Order Horn clauses).

%\url{https://goo.gl/c9vaXS} : Makefile
%## Outline Methodology: RQ, search strategy, selection criteria
This \SLR aims to answer the following \RQ: Among \ASC methods that do support preconditions and effects of services at an expressivity level higher than propositional, what is the level of expressivity achieved and how is this support achieved by the \ASC method. To answer this research question, two complementary search strategies where employed. First, the Scopus database was queried with a conjunction of four core terms (each specified as a disjunction of strongly related terms): \textit{service}, \textit{automatic}, \textit{composition} and \textit{precodition/effect}. Using a series of regular expressions\footnote{\url{https://goo.gl/eCE898},~~~\url{https://goo.gl/hqug2c}} the resulting 952 works were reduced to 172 that had their abstracts and full-text evaluated, resulting in 18 selected works. Second, highly influential venues\footnote{IEEE ICWS, IEEE SCC, ICSOC, IEEE TSC} had their proceedings and volumes from 2015 until 2017 manually scanned, which resulted in the inclusion of 5 additional works that the first search methodology was unable to select due to missing precondition-related terms in the abstract.

\section{Quantitative Analysis}
\label{sec:quant}

% theorem prover: 10
%    FOL: 5
%      ASP: 2
%      FOL(SAT):  1
%    Situation Calculus: 2
%    DDL: 2
%    N3: 1
% graph search: 11 
%    forward:  2
%    backward:  9
%    informed:  4
%        heuristic: 2
%    uninformed:  7
%        BFS: 4
%        DFS: 2
%    HTN: 2

Applying the selection strategy, 21 papers were selected, being 16 published in conferences and 5 in journals. 15 of these used subsets of \FOL to describe preconditions and effects, while 6 used \DLs. The most common approach, adopted by 11 papers, is to reduce the \ASC problem to path-finding in a graph. Most approaches (9) employ backward search (i.e., search starts from the goal state). Only 4 papers use informed search (2 with an heuristic function). Out of the 7 papers using uninformed search, 4 are breadth-first, 2 are depth-first and 1 lacked information. The second type of approach identified is reducing the \ASC problem to theorem proving. The approaches vary with respect to the particular logic of the theorem prover: 5 papers use subsets of \FOL, 2 papers use Situation Calculus \cite{mccarthy1963situations}, 2 papers use \DDL \cite{Chang2007}, and one paper uses \textit{N3Logic} \cite{n3Berners}.


\section{Discussion}
\label{sec:discussion}

%## Discuss main representative papers

% graph search - forward        :: mohr
% prover - FOL - SAT            :: alves
% prover - N3                   :: verborgh
% graph search - backward - DFS :: serrano
% DL                            :: CARSA

Mohr et al. \cite{Mohr2015} propose a composition algorithm that supports a subset of \FOL, to describe preconditions and effects. This subset does not support disjunction ($\vee$) and there is no quantification as all variables are also \IOs of the service. In addition to service descriptions, it is possible to describe a global knowledge base, limited to Horn clauses\footnote{Horn clauses have the form $p_1\ \wedge\ \ldots\ \wedge\ p_n \rightarrow q$ and are the basis for logic programming.}. The composition algorithm explores a graph of services backwards, from the goal state. Any service whose post-conditions satisfy partially these preconditions are predecessor nodes, which retain unsatisfied preconditions as preconditions of their own. This rule defines a graph were paths equate to service compositions, and a path from the goal node to a node without unsatisfied preconditions is a solution. For experiments the authors themselves designed two scenarios. Even though this benchmark is used only by the authors, results indicate a high computational cost for the consideration of preconditions and effects. For example, a composition of 10 services, from 100 relevant candidates takes 19 seconds in one scenario and 8 minutes in another.

Alves et al. \cite{Alves2016} tackle the problem of composing non-deterministic services, whose actual effects may be desirable or not. Preconditions are modeled as a conjunction and effects as a disjunction of desirable and undesirable effects, each modeled by conjunction. The algorithm reduces the composition problem to a non-deterministic planning problem, which is itself reduced to the Boolean satisfiability problem. The planning output is a contingency tree which determines how to proceed if, during execution, a service produces undesirable effects. This approach allows generating a \BPEL process that superimposes all alternative plans and can run in any \BPEL engine. On the other hand, the computational effort for dealing with the undesirable behavior of all services is paid at composition time regardless of whether these services misbehave or not.

Verborgh et al. \cite{Verborgh2017} propose an algorithm that interleaves planning and execution in order to support the \HATEOAS constraint of the \REST architectural style\footnote{Under such constraint, the client of a RESTful service must determine its next action from hypermedia controls contained in the representation received after interaction with a resource.}. Planning is reduced to theorem proving under \textit{N3Logic} \cite{n3Berners} by modeling services as implications under this logic. Preconditions and effects are restricted to conjunction of binary predicates (i.e., triple patterns such as \ms{?x a foaf:Person} that would be expressed as $a(x,\ Person)$ in \FOL). The proof, given as tree of implication applications, determines a workflow. After every interaction, results are added to a knowledge base and the proof is updated, yielding an updated plan. Again, the authors themselves constructed a benchmark, which consists in linking available services in a invocation chain. Composition times remain below one second in most cases. However, a particular property of this benchmark is that I/Os of a service are described so that each service has only a single suitable predecessor, which provides linear search space if this problem were to be solved with graph search.

Serrano et al. \cite{Serrano2017} propose a middleware where services are described by a single \RDF graph, with inputs and outputs of the service mapped to nodes of this graph. The end user requests a \SPARQL query, from which a desired graph is extracted. Employing graph isomorphism, any service whose graph intersects with the desired graph is selected. If the selected services have unknown inputs, the algorithm expands the desired graph to satisfy them and recurses, until the desired graph is completely satisfied. The level of support of preconditions and effects in this approach is similar to that in Pragmatic Proof. A limitation of this approach is that, by design, services with side effects (that modify data) can only be directly queried, and not included in a composition. Finally, the authors evaluated several properties of the middleware and demonstrate its applicability, but do not evaluate its scalability.

Niu et al. \cite{Niu2011} propose a software architecture for \ASC that describes services as actions in \DDL. Filtering algorithms remove services that are irrelevant for a composition problem and cluster similar services, retaining only the best (cf. user preferences) service in each cluster. The composition iteratively tries to add actions to a plan while the \DDL reasoner verifies if they can be added and if the goal remains achievable. In experiments the authors employ a well-known public dataset (OWL-S TC). A major drawback of this approach is that a composition request involving only two services could require up to 18 seconds, depending on preference constraints and initial state.

\autoref{tb:comparison} summarizes the logic formalisms employed by each of the papers discussed above. Most support only a subset of a logical language, a situation denoted with $\subset$. The second column reports whether disjunction is supported without restrictions (Yes), restricted to an element or if not supported (No). The same convention is used for the Negation column. Finally, a brief classification of each approach is given. All approaches in \autoref{tb:comparison} support some form of reasoning similar to the one possible with ontologies. \cite{Mohr2015} employ Horn clauses to this end, while \cite{Alves2016} considers equivalence relations; others support \OWL or \DL ontologies. 

\begin{table}
  \centering
  \caption{Comparison between representative works.}
  \label{tb:comparison}
  \setlength{\tabcolsep}{3pt}
  \begin{tabular}{lrccl}
    \toprule
    Work             & Logics  & Disj. & Negation & Approach   \\
    \midrule
    \cite{Mohr2015}  & $\subset$ \FOL
                     & No
                     & Literals
                     & Informed path finding \\
    \cite{Alves2016} & $\subset$ \FOL
                     & Effects
                     & Literals
                     & Theorem proof (SAT solver) \\
    \cite{Verborgh2017} & $\subset$ N3Logic
                     & No
                     & No
                     & Theorem proof \\
    \cite{Serrano2017} & $\subset$ \SPARQL
                     & No
                     & No
                     & Depth-first path-finding   \\
    \cite{Niu2011} & \DDL
                     & Yes
                     & Yes
                     & Theorem proof \\
    \bottomrule
  \end{tabular}
\end{table}

\bibliographystyle{sbc}
\bibliography{main}

\end{document}

%  LocalWords:  SBC resumo discoverable ASC NFPs Logics expressivity
%  LocalWords:  Scopus disjunction precodition ICWS SCC ICSOC TSC et
%  LocalWords:  Mohr al Alves NuPDDL satisfiability Verborgh foaf Niu
%  LocalWords:  middleware isomorphism recurses iteratively lrccl
%  LocalWords:  Disj
